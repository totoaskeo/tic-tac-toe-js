import * as PIXI from 'pixi.js'
import Game from './Game'

const app = new PIXI.Application({
  antialias: true,
  width: 600,
  height: 600
})
document.body.appendChild(app.view)

const game = new Game(app)
game.setup()
