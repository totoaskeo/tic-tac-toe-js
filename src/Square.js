import * as PIXI from 'pixi.js'
import TurnTypes from './TurnTypes'
import theme from './theme'

export default class extends PIXI.Graphics {
  constructor (coordinates, game) {
    super()
    this.game = game
    this.sign = null
    this.coordinates = coordinates
    this.lineStyle(theme.square.borderWidth, theme.square.borderColor)
    this.beginFill(theme.square.background)
    this.drawRect(coordinates.x, coordinates.y, coordinates.size, coordinates.size)
    this.endFill()
    this.interactive = true
    this.on('pointerdown', this.onPointerDown)
  }

  onPointerDown () {
    this.sign = this.game.turn
    if (this.sign === TurnTypes.CROSSES) {
      this.drawCross()
    } else if (this.sign === TurnTypes.NOUGHTS) {
      this.drawNought()
    }
    this.interactive = false

    this.game.toggleTurn()
    this.game.checkGameEnd()
  }

  drawCross () {
    const line = new PIXI.Graphics()
    line.lineStyle(theme.sign.width, theme.sign.color)
    line.moveTo(this.coordinates.x, this.coordinates.y)
    line.lineTo(this.coordinates.x + this.coordinates.size,
      this.coordinates.y + this.coordinates.size)
    this.game.gameScene.addChild(line)
    line.moveTo(this.coordinates.x + this.coordinates.size, this.coordinates.y)
    line.lineTo(this.coordinates.x, this.coordinates.y + this.coordinates.size)
    this.game.gameScene.addChild(line)
  }

  drawNought () {
    const ellipse = new PIXI.Graphics()
    ellipse.lineStyle(theme.sign.width, theme.sign.color)
    ellipse.drawEllipse(
      this.coordinates.x + this.coordinates.size / 2,
      this.coordinates.y + this.coordinates.size / 2,
      this.coordinates.size * 0.8 / 2,
      this.coordinates.size / 2
    )
    this.game.gameScene.addChild(ellipse)
  }
}
