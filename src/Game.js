import * as PIXI from 'pixi.js'
import settings from './settings'
import Square from './Square'
import TurnTypes from './TurnTypes'
import theme from './theme'

export default class {
  constructor (app) {
    this.app = app
    this.gameScene = new PIXI.Container()
    this.gameEndScene = new PIXI.Container()
    this.app.stage.addChild(this.gameScene)
    this.app.stage.addChild(this.gameEndScene)
    this.gameEndScene.visible = false
    this.turn = TurnTypes.CROSSES
    this.squares = []
  }

  setup () {
    this.setupGameScene()
    this.setupGameEndScene()
  }

  setupGameScene () {
    this.squares = []
    const appWidth = this.app.renderer.view.width
    const squareSize = (appWidth - 2 * settings.squareMargin) / settings.gameDimension
    for (let i = 0; i < settings.gameDimension; i++) {
      const squareRow = []
      for (let j = 0; j < settings.gameDimension; j++) {
        const coordinates = {
          x: settings.squareMargin + i * squareSize,
          y: settings.squareMargin + j * squareSize,
          size: squareSize
        }
        const square = new Square(coordinates, this)
        this.gameScene.addChild(square)
        squareRow.push(square)
      }
      this.squares.push(squareRow)
    }
  }

  setupGameEndScene () {
    this.gameEndScene.children.splice(0, this.gameEndScene.children.length)
    const bgrect = new PIXI.Graphics()
    bgrect.beginFill(theme.gameEnd.background, theme.gameEnd.alpha)
    bgrect.drawRect(0, 0, this.app.renderer.view.width, this.app.renderer.view.height)
    bgrect.endFill()
    this.gameEndScene.addChild(bgrect)

    const buttonStartNewGame = new PIXI.Sprite(PIXI.Texture.WHITE)
    buttonStartNewGame.tint = theme.buttonStartNewGame.background
    buttonStartNewGame.position.x = this.app.renderer.view.width / 3
    buttonStartNewGame.position.y = this.app.renderer.view.height / 3
    buttonStartNewGame.width = theme.buttonStartNewGame.width
    buttonStartNewGame.height = theme.buttonStartNewGame.height
    buttonStartNewGame.interactive = true
    buttonStartNewGame.buttonMode = true
    this.gameEndScene.addChild(buttonStartNewGame)

    const text = new PIXI.Text('Play again', {
      fontFamily: theme.gameEnd.text.fontFamily,
      fontSize: theme.gameEnd.text.fontSize,
      fill: theme.gameEnd.text.fill
    })
    text.position.x = buttonStartNewGame.position.x + 10
    text.position.y = buttonStartNewGame.position.y + 5
    this.gameEndScene.addChild(text)
    buttonStartNewGame.on('pointerdown', this.startNewGame.bind(this))
  }

  startNewGame () {
    this.gameEndScene.visible = false
    this.turn = TurnTypes.CROSSES
    this.setup()
  }

  toggleTurn () {
    this.turn = this.turn === TurnTypes.CROSSES ? TurnTypes.NOUGHTS : TurnTypes.CROSSES
  }

  checkGameEnd () {
    let squaresToCheck
    let gameEnded = false
    let message
    for (let i = 0; i < settings.gameDimension; i++) {
      // rows win
      squaresToCheck = this.squares.map(ss => ss[i])
      if (this.checkSquares(squaresToCheck)) {
        message = `${squaresToCheck[0].sign} wins on row ${i + 1}`
        gameEnded = true
      }
      // cols win
      squaresToCheck = this.squares[i]
      if (this.checkSquares(squaresToCheck)) {
        message = `${squaresToCheck[0].sign} wins on col ${i + 1}`
        gameEnded = true
      }
    }
    // main diagonal win
    squaresToCheck = this.squares.map((ss, index) => ss[index])
    if (this.checkSquares(squaresToCheck)) {
      message = `${squaresToCheck[0].sign} wins on main diagonal`
      gameEnded = true
    }
    // other diagonal win
    squaresToCheck = this.squares.map((ss, index) => ss[settings.gameDimension - 1 - index])
    if (this.checkSquares(squaresToCheck)) {
      message = `${squaresToCheck[0].sign} wins on other diagonal`
      gameEnded = true
    }
    // draw
    if (this.squares.every(ss => ss.every(s => s.sign))) {
      message = `it's a draw`
      gameEnded = true
    }

    if (gameEnded) {
      this.handleGameEnd(message)
    }
  }

  handleGameEnd (message) {
    this.squares.forEach(ss => ss.forEach(s => { s.interactive = false }))
    let style = new PIXI.TextStyle({
      fontFamily: theme.gameEnd.text.fontFamily,
      fontSize: theme.gameEnd.text.fontSize,
      fill: theme.gameEnd.text.fill
    })
    const text = new PIXI.Text(message, style)
    text.x = settings.squareMargin
    text.y = this.app.renderer.view.height / 2
    this.gameEndScene.addChild(text)
    this.gameEndScene.visible = true
  }

  checkSquares (squares) {
    return squares.every(s => s.sign && s.sign === squares[0].sign)
  }
}
