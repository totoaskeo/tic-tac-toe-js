export default {
  square: {
    background: 0x999999,
    borderWidth: 4,
    borderColor: 0xffffff
  },
  // sign is either a square or a nought
  sign: {
    width: 4,
    color: 0x466799
  },
  gameEnd: {
    background: 0x555555,
    alpha: 0.55,
    text: {
      fontFamily: 'Futura',
      fontSize: 52,
      fill: 'white'
    }
  },
  buttonStartNewGame: {
    width: 300,
    height: 80,
    background: 0x724372
  }
}
